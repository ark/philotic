```bash

# Installation
cilium install --set ingressController.enabled=true,ingressController.loadbalancerMode=dedicated,envoy.enabled=true,l2announcements.enabled=true,l2podAnnouncements.interface=enp1s0,k8sClientRateLimit.qps=30,k8sClientRateLimit.burst=60,kubeProxyReplacement=true,k8sServiceHost=192.168.122.119,k8sServicePort=443,externalIPs.enabled=true,nodePort.directRoutingDevice=enp1s0,devices='{enp1s0}' --version "v1.15.1"
```
