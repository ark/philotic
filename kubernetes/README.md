Kubenetes deployment
====================

Deploy using ArchLinux kubeadm and flannel

```bash
export INVENTORY=kube1X.yml
ansible-playbook -i ${INVENTORY} install-kubernetes.yaml
ansible-playbook -i ${INVENTORY} flannel-init-kubernetes.yaml
```
