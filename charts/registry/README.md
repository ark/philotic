Distribution deployment
====================

Deploy a secure distribution registry

```bash
# Install deps
helm install cert-manager jetstack/cert-manager --namespace cert-manager --create-namespace --version v1.7.1 --set installCRDs=true

# Install chart
helm install distribution10 ./registry --set commonName=kube20

# Get certs
kubectl get secrets distribution10-registry-certs -o json | jq -r '.data."tls.crt"' | base64 -d > tls.crt
kubectl get secrets distribution10-registry-certs -o json | jq -r '.data."ca.crt"' | base64 -d > ca.crt

# Add certs
trust anchor tls.crt
trust anchor ca.crt
update-ca-trust
```
